# Praca magisterska

Repozytorium zawiera dwa foldery:

- alpha-zero-general-master - zawierający algorytm AlphaZero, wyuczone wagi sieci neuronowych, oraz program umożliwiający prowadzenie rozgrywki przez sieć neuronową
- candycrush-yolo-image-gen - zawierający pliki graficzne oraz program do wygenerowania danych uczących na potrzeby algorytmu YOLO, i plik "dataset.yaml" zawierający dane o obiektach możliwych do wykrycia przez algorytm.

Oraz dwa notatniki Jupyter Notebook, przeznaczone do wykorzystania na środowisku Google Colab:

- AlphaZeroTrainingSession.ipynb - zawierający kod do przeprowadzenia sesji uczenia algorytmu AlphaZero

- YoloTrainingSession.ipynb - zawierający kod do wygenerowania danych uczących i przeprowadzenia sesji uczenia algorytmu YOLO


## Proces uczenia sieci neuronowej YOLO

1. Uruchomić notatnik na środowisku Google Colab
2. Uruchomić pierwszą część notatnika
3. Umieścić w katalogu głównym całość katalogów "candies", "redherrings", i "training_imgs", razem z ich strukturą
4. Umieścić kod "training_image_dataset_producer_3.py" i "dataset.yaml" w katalogu "yolov5"
5. Uruchomić drugą część notatnika
6. (Opcjonalnie) Jeżeli istnieje plik z wyuczonymi wagami dla sieci neuronowej YOLO, umieścić go w katalogu "yolov5/weights"
7. Uruchomić trzecią część (jeżeli nie ma pliku z wyuczonymi wagami), albo czwartą część notatnika (jeśli jest)
8. Poczekać, aż sesja uczenia się skończy.
9. Pobrać wyuczone wagi i informacje zwrotne z katalogu "runs/train"


## Proces uczenia sieci neuronowej AlphaZero

1. (Opcjonalnie) Jeżeli planowane jest przeprowadzenie procesu uczenia od nowa, usunąć zawartość katalogu "weights", albo - jeżeli planowane jest wygenerowanie zupełnie nowych danych uczących - tylko plik "weights/best.pth.tar.examples"
2. Przekopiować zawartość katalogu "alpha-zero-general-master" na Dysk Google
3. Uruchomić notatnik na środowisku Google Colab, i aktywować połączenie z Dyskiem Google
4. Uruchomić kod notatnika
5. Poczekać, aż sesja uczenia się skończy
6. Pobrać wyuczone wagi i dane uczące z katalogu "/temp"


Uwagi:
- W katalogu "alpha-zero-general-master/matchthree/prototype" znajduje się plik "Board.py", a w nim - metody "initializeFromConfigurations()" i "calculateScoreThreshold()" - tam znajdują się dwa sposoby pobrania konfiguracji i progu punktów, jeden przeznaczony do procesu uczenia, drugi do procesu ewaluacji


## Proces ewaluacji sieci neuronowej na utworzonym prototypie

1. Uruchomić kod "pit.py"


## Proces ewaluacji sieci neuronowej na grze Candy Crush

1. Uruchomić kod "detectionAI.py"