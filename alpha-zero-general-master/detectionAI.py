import pyautogui
import pygetwindow as gw
from pynput import keyboard
from mss import mss
import torch
import numpy as np
import cv2
import math

import Arena
from MCTS import MCTS
from matchthree.Match3Game import Match3Game
from matchthree.Match3Players import *
from matchthree.keras.NNet import NNetWrapper as NNet

from my_utils import *

candyCrushClassToHash = [
    int('00011111010100101000', 2),
    int('00011111010100101100', 2),
    int('00011111011100101000', 2),
    int('00011111100100101000', 2),
    int('00011111100100100000', 2),
    int('00011111011000100000', 2),
    int('00011111110000100000', 2),
    int('00011111110100100000', 2),
    int('00011111101000101000', 2),
    int('00011111100000101000', 2),
    int('00000000111101000000', 2),
    int('00000000100011100100', 2),
    int('00001111111000111010', 2),
    int('00001111111100110000', 2),
    int('00000000001010100100', 2),
    int('00101111010100101000', 2),
    int('00101111010100101100', 2),
    int('00101111011100101000', 2),
    int('00101111100100101000', 2),
    int('00101111100100100000', 2),
    int('00101111011000100000', 2),
    int('00101111110000100000', 2),
    int('00101111110100100000', 2),
    int('00101111101000101000', 2),
    int('00101111100000101000', 2),
    int('00000000001110100100', 2),
    int('00000000010010100100', 2),
    int('00000000010110100100', 2),
    int('00000000011010100100', 2),
    int('00000000011110100100', 2),
    int('00000000011100100100', 2),
    int('00001111100010100100', 2),
    int('00111111010100101000', 2),
    int('00111111010100101100', 2),
    int('00111111011100101000', 2),
    int('00111111100100101000', 2),
    int('00111111100100100000', 2),
    int('00111111011000100000', 2),
    int('00111111110000100000', 2),
    int('00111111110100100000', 2),
    int('00111111101000101000', 2),
    int('00111111100000101000', 2),
    int('01001111010100101000', 2),
    int('01001111010100101100', 2),
    int('01001111011100101000', 2),
    int('01001111100100101000', 2),
    int('01001111100100100000', 2),
    int('01001111011000100000', 2),
    int('01001111110000100000', 2),
    int('01001111110100100000', 2),
    int('01001111101000101000', 2),
    int('01001111100000101000', 2),
    int('01011111010100101000', 2),
    int('01011111010100101100', 2),
    int('01011111011100101000', 2),
    int('01011111100100101000', 2),
    int('01011111100100101000', 2),
    int('01011111011000100000', 2),
    int('01011111110000100000', 2),
    int('01011111110100100000', 2),
    int('01011111101000101000', 2),
    int('01011111100000101000', 2),
    int('01101111010100101000', 2),
    int('01101111010100101100', 2),
    int('01101111011100101000', 2),
    int('01101111100100101000', 2),
    int('01101111100100100000', 2),
    int('01101111011000100000', 2),
    int('01101111110000100000', 2),
    int('01101111110100100000', 2),
    int('01101111101000101000', 2),
    int('01101111100000101000', 2),
    ]


def find_idx(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

titles = []
for x in pyautogui.getAllWindows():  
    if(len(x.title) > 0):
        print(x.title+" - "+str(len(titles)))
        titles.append(x.title) 
input1 = input("Write down a number of the window to activate: ")

w = gw.getWindowsWithTitle(titles[int(input1)])[0]

running = True
paused = False

def on_press(key):
    global paused
    try:
        if key.char == 'p':
            paused = True
        elif key.char == 'r':
            paused = False
    except AttributeError:
        pass
def on_release(key):
    global running
    if key == keyboard.Key.esc:
        running = False
        return False  
listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()

sct = mss()

model = torch.hub.load('ultralytics/yolov5', 'custom', path='candycrush/model.pt')
model.conf=0.93
w.minimize()
w.restore()
bounding_box = {'top': w.top, 'left': w.left + math.floor((w.width/5)),
                'width': w.width - math.floor((w.width/5)), 'height': w.height}

g = Match3Game(13)
n1 = NNet(g)
n1.load_checkpoint('./weights/','best.pth.tar')
args1 = dotdict({'numMCTSSims': 8, 'cpuct':1.0})
mcts1 = MCTS(g, n1, args1)
n1p = lambda x: np.argmax(mcts1.getActionProb(x, temp=0))
arena = Arena.Arena(n1p, n1p, g, display=Match3Game.display)

boardCopy = []
counter = 0
while(running):
    if(not paused):
        if(gw.getActiveWindow() == w):
            sct_img = sct.grab(bounding_box)
            scr_img = np.array(sct_img)
            scr_img = cv2.cvtColor(scr_img, cv2.COLOR_BGR2RGB)
            img = model(scr_img)
            avgXValues, avgYValues = [], []
            stepX = 0
            stepY = 0
            detectionList = ([t.detach().numpy() for t in img.xyxyn][0])
            if(len(detectionList) > 0):
                line = []
                previousVal = 0
                for i in sorted(detectionList, key = lambda s: s[1]):
                    if(not (abs(previousVal - i[1])  < 0.02 or previousVal == 0)):
                        avgYValues.append(sum(x[1] for x in line) / len(line))
                        line = []                        
                    line.append(i)
                    previousVal = i[1]
                avgYValues.append(sum(x[1] for x in line) / len(line))
                line = []
                previousVal = 0
                for i in sorted(detectionList, key = lambda s: s[0]):
                    if(not (abs(previousVal - i[0])  < 0.02 or previousVal == 0)):
                        avgXValues.append(sum(x[0] for x in line) / len(line))
                        line = []
                    line.append(i)
                    previousVal = i[0]
                avgXValues.append(sum(x[0] for x in line) / len(line))
                if (len(avgXValues) < 2 or len(avgYValues) < 2):
                    continue
                board = [[0 for i in avgXValues]for j in avgYValues]
                for i in detectionList:
                    board[find_idx(avgYValues, i[1])][find_idx(avgXValues, i[0])] = candyCrushClassToHash[int(i[5])]
                if(board == boardCopy and board is not None):
                    if (counter > 1):
                        action = arena.calculateNextAction(board, verbose = False)
                        line = int(action / 25)
                        digit = action % 25
                        useDownMove = digit - 12 >= 0
                        moveX = (digit - 12 if (useDownMove) else digit)
                        moveY = line                        
                        yPos = bounding_box['top'] + ((avgYValues[moveY]) * bounding_box['height'])+20
                        xPos = bounding_box['left'] + ((avgXValues[moveX]) * bounding_box['width'])+20
                        pyautogui.click(xPos, yPos)
                        pyautogui.dragRel(0 if (useDownMove) else 80, 80 if (useDownMove) else 0)
                        counter = 0                                      
                else:
                    counter = 0     
                boardCopy = board.copy()
                counter = counter + 1
        else:  
            w.minimize()
            w.restore()

