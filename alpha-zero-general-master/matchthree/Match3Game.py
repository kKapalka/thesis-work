from __future__ import print_function
import sys
sys.path.append('..')
from Game import Game
from .Match3Logic import Board
import numpy as np
import copy
import random
import cv2

"""
Game class implementation for the game of Match3.
"""
class Match3Game(Game):
    def __init__(self, n=15):
        self.n = n

    def getInitBoard(self, sameSeed = False):
        if(sameSeed):
            self.b = copy.deepcopy(self.bCopy)
            random.seed(self.b.get_seed())
        else:
            self.b = Board(self.n, 0, 0)
            self.bCopy = copy.deepcopy(self.b)
            random.seed(self.b.get_seed())
        # return initial board (numpy board)
        return np.array(self.b.pieces)

    def getBoardSize(self):
        # (a,b) tuple
        return (self.n, self.n)

    def getActionSize(self):
        # return number of actions
        return (self.n * (self.n - 1) * 2)

    def getNextState(self, board, player, action, movesPerformed = 0):
        # if player takes action on board, return next (board,player)
        # action must be a valid move
        b = Board(self.n, self.b.xOffset, self.b.yOffset, self.b.get_playout_stats(), self.b.colors, self.b.moveCount, self.b.seed)
        b.replicate(board, movesPerformed, self.b.get_seed())
        b.execute_move(action)
        return (b.pieces, player, b.getMovesPerformed())

    def calculateNextAction(self, board, player, action):
        b = Board(13, 0, 0, 0, 4, 0, 5000)
        b.replicate(board, 0, 5000)
        b.executeMove(action)
        return (b.pieces, player, b.getMovesPerformed())

    def performNextAction(self, board, player, action):
        # if player takes action on board, return next (board,player)
        # action must be a valid move
        self.b.execute_move(action)
        return (self.b.pieces, player, self.b.getMovesPerformed())

    def getValidMoves(self, board, player, verbose = False):
        b = Board(self.n, self.b.xOffset, self.b.yOffset, self.b.get_playout_stats(), self.b.colors, self.b.moveCount, self.b.seed)
        b.replicate(board, 0, self.b.get_seed())
        legalMoves = b.get_legal_moves(verbose)
        if(verbose):
            print(legalMoves)
        validMoves = [0] * self.getActionSize()
        for i in legalMoves:
            validMoves[i] = 1
        # return a fixed size binary vector
        return validMoves

    def getScore(self):
        return self.b.get_score()

    def getGameEnded(self, board, player, movesPerformed = 0):
        
        b = Board(self.n, self.b.xOffset, self.b.yOffset, self.b.get_playout_stats(), self.b.colors, self.b.moveCount, self.b.seed)
        b.replicate(board, movesPerformed, self.b.get_seed())
        return b.is_win()

    def getPlayoutStats(self):
        return self.b.get_playout_stats(), self.b.is_win()

    def getCanonicalForm(self, board, player):
        # return state if player==1, else return -state if player==-1
        return board
    def getSymmetries(self, board, pi):
        #mirror, rotational
        xOffset = self.b.xOffset
        yOffset = self.b.yOffset
        xPadding = self.n - (self.b.xOffset + self.b.width - 1)
        yPadding = self.n - (self.b.yOffset + self.b.height - 1)
        xMoveShift = 1
        yMoveShift = (self.n + (self.n - 1))
        l = []

        for j in range(0, yOffset + 1):
            board_copy = np.roll(board, j * -1, axis = 0)
            pi_copy = np.roll(pi, j * yMoveShift * -1, axis = 0)
            for i in range(1, xOffset + 1):
                board_copy_2 = np.roll(board_copy, i * -1, axis = 1)
                pi_copy_2 = np.roll(pi_copy, i * xMoveShift * -1, axis = 0)
                l += [(board_copy_2, pi_copy_2)]
        
        for j in range(0, yPadding ):
            board_copy = np.roll(board, j, axis = 0)
            pi_copy = np.roll(pi, j * yMoveShift, axis = 0)
            for i in range(1, xPadding):
                board_copy_2 = np.roll(board_copy, i, axis = 1)
                pi_copy_2 = np.roll(pi_copy, i * xMoveShift, axis = 0)
                l += [(board_copy_2, pi_copy_2)]

        return l

    def stringRepresentation(self, board, moveNumber = 0):
        # 8x8 numpy array (canonical board)
        return str(board.tolist())+','+str(moveNumber)
    
    @staticmethod
    def display(board):
        print(board)
        colors = [
            [0, 0, 0],
            [255, 0, 0],
            [0, 255, 0],
            [0, 0, 255],
            [255, 255, 0],
            [255, 0, 255],
            [0, 255, 255],
            [70, 160, 70],
            [160, 70, 70],
            [70, 70, 160]
            ]
        image = [[[0, 0, 0] for i in range(13*35)]for j in range(13*35)]
        for row in range(len(board)):
            for col in range(len(board[row])):
                binary = bin(board[row][col])[2:].zfill(20)
                end = len(binary) - 17
                start = end - 3
                kBitSubStr = binary[start : end+1]
            
                color = int(kBitSubStr, 2)
                for i in range(30):
                    for j in range(30):
                        image[row * 35 + i][col*35+j] = colors[color]
        cv2.imshow("board", np.array(image, dtype=np.uint8))
        cv2.waitKey(1)
        
