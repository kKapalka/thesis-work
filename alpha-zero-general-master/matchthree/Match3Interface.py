import numpy
from .prototype.Board import Board
from .prototype.BoardElement import BoardElement
import random


class Match3Interface(object):

    def extractBitData(self, code, bits, pos, baseBitLength = 20):
        binary = bin(code)[2:].zfill(baseBitLength)
        end = len(binary) - pos
        start = end - bits + 1
        kBitSubStr = binary[start : end+1]
        return int(kBitSubStr, 2)

    def __init__(self, maxSize = 15, xOffset = None, yOffset = None, colors = 0, moveCount = 0, seed = 0):
        self.maxSize = maxSize
        self.xOffset = xOffset
        self.yOffset = yOffset
        self.maximumMoveNumber = (self.maxSize * (self.maxSize - 1) * 2) - 1
        self.matchBoard = [[[False, False, False, False] for i in range(self.maxSize)] for j in range(self.maxSize)]
        self.initialize()
        if(colors > 0):
            self.colors = colors
        if(moveCount > 0):
            self.moveCountThreshold = moveCount
        if(seed > 0):
            self.seed = seed

    def initialize(self):
        self.board = self.getBoard()
        if(self.xOffset is None):
            xOffset = random.randrange(0, self.maxSize - self.board.width)
            self.xOffset = min(xOffset, self.maxSize - self.board.width)
        if(self.yOffset is None):
            yOffset = random.randrange(0, self.maxSize - self.board.height)
            self.yOffset = min(yOffset, self.maxSize - self.board.height)
        self.width = self.board.width
        self.height = self.board.height
        self.moveCountThreshold = self.board.moveCountThreshold
        self.seed = self.board.seed
        self.colors = self.board.validColors
    
    def replicate(self, board, moves, score):
        self.board = Board()
        self.board.initializeFromBoardState(board, moves, self.moveCountThreshold, score, self.seed, self.colors)
        self.width = self.board.width
        self.height = self.board.height
        

    def getMovesPerformed(self):
        return self.board.moveCount

    def getBoard(self):
        board = Board()
        board.initializeFromConfigurations()
        return board

    def createHashBoard(self):
        self.serializedBoard = []
        for i in range(0, self.maxSize):
            line = []
            for j in range(0, self.maxSize):
                if (j - self.xOffset < 0 or j - self.xOffset >= self.board.width):
                    line.append(0)
                else:
                    if(i - self.yOffset < 0 or i - self.yOffset >= self.board.height):
                        line.append(0)
                    else:
                        line.append(self.board.getCode(i - self.yOffset,j - self.xOffset))
            self.serializedBoard.append(line)
        self.serializedBoard = numpy.array(self.serializedBoard)

    def readBoardState(self):
        self.createHashBoard()

    def getActionSpace(self):
        return self.maximumMoveNumber + 1

    def getLowHigh(self):
        return BoardElement.getLowHigh()

    def getScore(self):
        return self.board.score

    def getBoardState(self):
        self.createHashBoard()
        return self.serializedBoard

    def getValidMoves2(self):
        self.createHashBoard()
        validMoves = []
        for moveNumber in range(self.getActionSpace()):
            line = int(moveNumber / (self.maxSize * 2 - 1))
            digit = moveNumber % (self.maxSize * 2 - 1)
            useDownMove = digit - (self.maxSize - 1) > 0

            moveX = (digit - (self.maxSize - 1) if (useDownMove) else digit)
            moveY = line
            boardElementCode = self.serializedBoard[moveY][moveX]
            if(boardElementCode != 0):
                if(self.extractBitData(boardElementCode, 1, 15) and not useDownMove):
                    secondMovedElement = self.serializedBoard[moveY][moveX]
                    if(self.extractBitData(boardElementCode, 1, 13)):
                        validMoves.append(moveNumber)
                elif(self.extractBitData(boardElementCode, 1, 14) and useDownMove):
                    secondMovedElement = self.serializedBoard[moveY][moveX]
                    if(self.extractBitData(boardElementCode, 1, 16)):
                        validMoves.append(moveNumber)
                    validMoves.append(moveNumber)
        return validMoves

    def getValidMoves(self, verbose = False):
        self.createHashBoard()
        if verbose:
            print("finding")
            self.board.findPotentialMatches()
            self.board.print()
        validMoves = []
        for i in range(0, self.maxSize):
            for j in range(0, self.maxSize):
                self.matchBoard[j][i] = [False, False, False, False]
        for i in range(0, self.maxSize):
            for j in range(0, self.maxSize):
                boardElementCode = self.serializedBoard[j][i]
                checkMatrix = []                
                if(j < self.maxSize - 1):
                    otherChecked = self.serializedBoard[j+1][i]
                    if(self.canCreateMatch(boardElementCode, otherChecked)):
                        checkMatrix.append((otherChecked, [check for check in [
                            ([j-1, i-1], 1),
                            ([j-1, i+1], 3),
                            ([j - 2, i], 2),
                            ([j+2, i-1], 1),
                            ([j+2, i+1], 3),
                            ([j + 3, i], 0)
                        ] if check[0][0] >= 0 and check[0][0] < self.maxSize and check[0][1] >= 0 and check[0][1] < self.maxSize ]))
                if(j < self.maxSize - 2):
                    otherChecked = self.serializedBoard[j+2][i]
                    if(self.canCreateMatch(boardElementCode, otherChecked)):
                        checkMatrix.append((otherChecked, [check for check in [
                            ([j+1, i-1], 1),
                            ([j+1, i+1], 3)
                        ] if check[0][1] >= 0 and check[0][1] < self.maxSize]))
                if(i < self.maxSize - 1):
                    otherChecked = self.serializedBoard[j][i+1]
                    if(self.canCreateMatch(boardElementCode, otherChecked)):
                        checkMatrix.append((otherChecked, [check for check in [
                            ([j-1, i-1], 2),
                            ([j+1, i-1], 0),
                            ([j, i - 2], 1),
                            ([j-1, i+2], 2),
                            ([j+1, i+2], 0),
                            ([j, i + 3], 3),
                        ] if check[0][0] >= 0 and check[0][0] < self.maxSize and check[0][1] >= 0 and check[0][1] < self.maxSize ]))
                if(i < self.maxSize - 2):
                    otherChecked = self.serializedBoard[j][i+2]
                    if(self.canCreateMatch(boardElementCode, otherChecked)):
                        checkMatrix.append((otherChecked, [check for check in [
                            ([j-1, i+1], 2),
                            ([j+1, i+1], 0)
                        ] if check[0][0] >= 0 and check[0][0] < self.maxSize]))
                for check in checkMatrix:
                    otherChecked = check[0]
                    for checkField in check[1]:
                        candidate = self.serializedBoard[checkField[0][0]][checkField[0][1]]
                        if(self.canCreateMatch(boardElementCode,candidate) and self.canCreateMatch(candidate,otherChecked)):
                            x = checkField[0][1]
                            y = checkField[0][0]
                            if(verbose):
                                print(x, y, checkField[1], 13 + ((5 - checkField[1])% 4))
                            if (checkField[1] == 0):
                                y = y - 1
                            elif (checkField[1] == 1):
                                x = x + 1
                            elif (checkField[1] == 2):
                                y = y + 1
                            elif (checkField[1] == 3):
                                x = x - 1
                            if(verbose):
                                print(x, y, self.extractBitData(self.serializedBoard[y][x], 1, 13 + ((5 - checkField[1])% 4)))
                                print(self.extractBitData(self.serializedBoard[y][x], 1, 13 + ((5 - checkField[1])% 4)) == 1)
                                
                            matchFound = self.extractBitData(self.serializedBoard[y][x], 1, 13 + ((5 - checkField[1])% 4)) == 1
                            self.matchBoard[checkField[0][0]][checkField[0][1]][checkField[1]] = self.matchBoard[checkField[0][0]][checkField[0][1]][checkField[1]] or matchFound
                            if(verbose):
                                print(self.matchBoard[checkField[0][0]][checkField[0][1]][checkField[1]])

        for y in range(len(self.matchBoard)):
            for x in range(len(self.matchBoard[0])):
                matchElement = self.matchBoard[y][x]
                if(any(matchElement)):
                    moveNumber = y * (self.maxSize * 2 - 1) + x
                    if(verbose):
                        print(y, x, matchElement, moveNumber)
                    if(matchElement[0]):
                        copyMoveNumber = moveNumber - self.maxSize
                        validMoves.append(copyMoveNumber)
                    if(matchElement[1]):
                        validMoves.append(moveNumber)
                    if(matchElement[2]):
                        copyMoveNumber = moveNumber + (self.maxSize - 1)
                        validMoves.append(copyMoveNumber)
                    if(matchElement[3]):
                        copyMoveNumber = moveNumber - 1
                        validMoves.append(copyMoveNumber)
                    
        return list(set(validMoves))
        
    
    def canCreateMatch(self, code1, code2):
        code1color = self.extractBitData(code1, 4, 17)
        code2color = self.extractBitData(code2, 4, 17)
        code1canMatch = self.extractBitData(code2, 1, 5)
        code2canMatch = self.extractBitData(code2, 1, 5)
        return (code1canMatch == 1 or code2canMatch == 1 or code1color == code2color) and max(code1color, code2color) != 0
        

    def getVictoryState(self):
        return self.board.getVictoryState()

    def performMatrixMove(self, moveNumber):
        if(moveNumber < 0 or moveNumber > self.maximumMoveNumber):
            print("Move number outside of bracket <0, "+str(self.maximumMoveNumber)+">: "+str(moveNumber))
            return
        ## matrix is expected to allow navigation on a board up to 13x13 in size
        ## moveNumber corresponds to both element of interest, and direction of move
        ## if move cannot be performed one way, it will be attempted the opposite way
        ## (as in, instead of moving a lower element up, upper element will be moved down)
        ## 25 moves per line - 12 moves to the right, 13 moves down. 12 lines. + 1 line of 12 elements. = 12 * 13 * 2 = 312 different possible moves
        line = int(moveNumber / (self.maxSize * 2 - 1))
        digit = moveNumber % (self.maxSize * 2 - 1)
        useDownMove = digit - (self.maxSize - 1) >= 0
        moveX = (digit - (self.maxSize - 1) if (useDownMove) else digit) - self.xOffset
        moveY = line - self.yOffset
        #print("Moving element ["+str(moveX)+", "+str(moveY)+"] - "+("down" if (useDownMove) else "right"))
        if(not self.board.shift(moveX, moveY, ("down" if (useDownMove) else "right"))):
            alternateMove = False
            if(useDownMove):
                #print("Alternate move of element ["+str(moveX)+", "+str(moveY+1)+"] - up")
                alternateMove = self.board.shift(moveX, moveY + 1, "up")
            else:
                #print("Alternate move of element ["+str(moveX+1)+", "+str(moveY)+"] - left")
                alternateMove = self.board.shift(moveX + 1, moveY, "left")
            return alternateMove
        return True
