import numpy as np

class RandomPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        a = np.random.randint(self.game.getActionSize())
        valids = self.game.getValidMoves(board, 1)
        while valids[a]!=1:
            a = np.random.randint(self.game.getActionSize())
        return a


class HumanMatch3Player():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        # display(board)
        print(self.game.getCanonicalForm(board, 1))
        valid = self.game.getValidMoves(board, 1)
        for i in range(len(valid)):
            if(valid[i] == 1):
                line = int(i / (self.game.n * 2 - 1))
                digit = i % (self.game.n * 2 - 1)
                useDownMove = digit - (self.game.n - 1) >= 0

                moveX = (digit - (self.game.n - 1) if (useDownMove) else digit)
                moveY = line
                print(str(moveX+1)+", "+str(moveY+1)+" - "+str(i) + " - " + ("down" if useDownMove else "right"))
        while True: 
            # Python 3.x
            a = input()
            # Python 2.x 
            # a = raw_input()
            try:
                a = int(a)
            except Exception:
                a = -1
            if a > len(valid):
                print('Invalid')
            else:
                if valid[a]:
                    break
                else:
                    print('Invalid')

        return a
