import sys
sys.path.append('..')
from my_utils import *

import argparse
from keras.models import *
from keras.layers import *
from keras.optimizers import *

class Match3NNet():
    def __init__(self, game, args):
        # game params
        self.board_x, self.board_y = game.getBoardSize()
        self.action_size = game.getActionSize()
        self.args = args

        # Neural Net
        self.input_boards = Input(shape=(self.board_x, self.board_y))    

        x_image = Reshape((self.board_x, self.board_y, 1))(self.input_boards)                
        layer = Activation('relu')(BatchNormalization(axis=3)(Conv2D(args.num_channels, 3, padding='same')(x_image)))         
        for _ in range(14):
            res = layer
            layer = Activation('relu')(Conv2D(args.num_channels, 3, padding='same')(layer))
            layer = BatchNormalization(axis=3)(layer)
            layer = Add()([((Conv2D(args.num_channels, 3, padding='same')(layer))), res])
            layer = ReLU()(layer)
            layer = BatchNormalization(axis=3)(layer)             
        h_conv4_flat = Flatten()(layer)       
        s_fc1 = Dropout(args.dropout)(Activation('relu')(BatchNormalization(axis=1)(Dense(1024)(h_conv4_flat))))  
        s_fc2 = Dropout(args.dropout)(Activation('relu')(BatchNormalization(axis=1)(Dense(512)(s_fc1))))          
        self.pi = Dense(self.action_size, activation='softmax', name='pi')(s_fc2)   
        self.v = Dense(1, activation='tanh', name='v')(s_fc2)                    

        self.model = Model(inputs=self.input_boards, outputs=[self.pi, self.v])
        self.model.compile(loss=['categorical_crossentropy','mean_squared_error'], optimizer=adam_v2.Adam(args.lr))
