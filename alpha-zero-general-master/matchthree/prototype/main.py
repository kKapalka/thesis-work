from Board import Board
from BoardElement import BoardElement,StandardColorElement, Freckles, VerticalStripey, HorizontalStripey, Icing1

def main():
    for i in range(0, 100000):
        boardLines = []
        boardLines.append("121342")
        boardLines.append("411341")
        boardLines.append("334113")
        boardLines.append("II414I")
        boardLines.append("IIIIII")
        boardLines.append("IIIIII")
        boardLines.append("IIIIII")
        boardLines.append("1IIII1")
        board = Board()
        board.initializeFromLines(boardLines)
        moveCount = 0
        while(board.getVictoryState() == 0):
            moveCount = moveCount + 1
            board.performRandomSwap()


if __name__ == "__main__":
    main()
