class BoardElement(object):
    def __init__(self, color, board, x, y):
        self.color = color
        self.validMoves = [True, True, True, True]
        self.reactToMatchNearby = False
        self.power = 5
        self.hasEndOfTurnEffects = False
        self.respondsToGravity = True
        self.canBeMatchedWithEveryColor = False
        self.canSpawnNewElementOnMatch = False
        self.matchLogicExecuted = False
        self.prohibitsVictoryInCurrentState = False
        self.cellProhibitsVictoryInCurrentState = False
        self.canBeMovedIntoWithoutCreatingAMatch = False
        self.canBeRemoved = True
        
        self.x = x
        self.y = y
        self.board = board
        self.matchesUntilDestruction = 1
        self.matchedHorizontally = False
        self.matchedVertically = False
        self.wouldReactToMatchNearby = False
        self.moveWouldResultInAMatch = [False, False, False, False]

    
    def createHash(self):
        myHash = self.color
        for move in self.validMoves:
            myHash = myHash * 2 + (1 if move else 0)
        elementPower = max(0, min(15, self.power))
        myHash = myHash * 16 + elementPower
        for condition in [self.reactToMatchNearby, self.hasEndOfTurnEffects, self.respondsToGravity, self.canBeMatchedWithEveryColor, self.canSpawnNewElementOnMatch]:
            myHash = myHash * 2 + (1 if condition else 0)
        for condition in [self.prohibitsVictoryInCurrentState, self.canBeMovedIntoWithoutCreatingAMatch, self.cellProhibitsVictoryInCurrentState]:
            myHash = myHash * 2 + (1 if condition else 0)
        return myHash        

    def swap(self, other):
        xDiff = self.x - other.x
        yDiff = self.y - other.y
        if(self.color == other.color and self.color != 0):
            return False
        if(yDiff != 0):
            if(not (self.validMoves[1 - yDiff] and other.validMoves[1 + yDiff])):
                return False
        elif(xDiff != 0):
            if(not (self.validMoves[2 - xDiff] and other.validMoves[2 + xDiff])):
                return False
        else:
            return False
        return self.board.attemptMove(self, other)

    def match(self, color):
        if(self.canBeRemoved):
            if(not self.matchLogicExecuted):
                self.matchLogicExecuted = True
                if(self.color == 0 and color != 0):
                    self.color = color
                self.matchesUntilDestruction = max(self.matchesUntilDestruction - 1, 0)
                self.executeMatchLogic()
                self.board.toggleNearbyMatchFor(self.x, self.y)
            
    def executeMatchLogic(self):
        pass

    def onMatchNearby(self):
        if(self.reactToMatchNearby and self.wouldReactToMatchNearby):
            self.executeMatchNearbyLogic()

    def executeMatchNearbyLogic(self):
        pass

    def onEndTurn(self):
        self.matchLogicExecuted = False
        self.wouldReactToMatchNearby = False
        if(self.hasEndOfTurnEffects):
            self.executeEndTurnLogic()
    
    def executeEndTurnLogic(self):
        pass

    def conditionallySpawnNewElement(self, color, matchSize):
        pass

    def canBeMatchedWith(self, other):
        return (self.canBeMatchedWithEveryColor or other.canBeMatchedWithEveryColor or self.color == other.color) and max(self.color, other.color) != 0
        
class Blocker(BoardElement):
    def __init__(self, color, board, x, y):
        BoardElement.__init__(self, 0, board, x, y)
        self.validMoves = [False, False, False, False]
        self.respondsToGravity = False
        self.power = 0
        self.canBeRemoved = False
        

class StandardColorElement(BoardElement):
    def __init__(self, color, board, x, y):
        BoardElement.__init__(self, color, board, x, y)
        self.canSpawnNewElementOnMatch = True

    def conditionallySpawnNewElement(self, color, matchSize):
        if(matchSize == 5):
            self.board.spawnFreckles(self.x, self.y)
        elif(self.matchedHorizontally and self.matchedVertically):
            self.board.spawnWrappedCandy(self.x, self.y, color)
        elif(matchSize == 4):
            self.board.spawnStripey(self.x, self.y, color, "H" if (self.matchedHorizontally) else "V")

class Freckles(StandardColorElement):
    def __init__(self, color, board, x, y):
        BoardElement.__init__(self, color, board, x, y)
        self.color = 0
        self.canBeMatchedWithEveryColor = True
        self.power = 15

    def executeMatchLogic(self):
        for i in range(0, self.board.height):
            for j in range(0, self.board.width):
                if (self.board.board[i][j].color == self.color and i != self.y and j != self.x):
                    self.board.board[i][j].match(0)

class HorizontalStripey(StandardColorElement):
    def __init__(self, color, board, x, y):
        StandardColorElement.__init__(self, color, board, x, y)
        self.power = 10

    def executeMatchLogic(self):
        for i in range(0, self.board.width):
            if (i != self.x):
                self.board.board[self.y][i].match(self.color)

class WrappedCandy(StandardColorElement):
    def __init__(self, color, board, x, y):
        StandardColorElement.__init__(self, color, board, x, y)
        self.power = 7

    def executeMatchLogic(self):
        for i in range(max(0, self.x-1), min(self.x+1, self.board.width)):
            for j in range(max(0, self.y-1), min(self.y+1, self.board.height)):
                if(i != self.x and j != self.y):
                    self.board.board[j][i].match(self.color)

class VerticalStripey(StandardColorElement):
    def __init__(self, color, board, x, y):
        StandardColorElement.__init__(self, color, board, x, y)
        self.power = 8

    def executeMatchLogic(self):
        for i in range(0, self.board.height):
            if (i != self.y):
                self.board.board[i][self.x].match(self.color)

class Icing1(BoardElement):
    def __init__(self, color, board, x, y):
        BoardElement.__init__(self, color, board, x, y)
        self.color = 0
        self.power = 3
        self.validMoves = [False, False, False, False]
        self.reactToMatchNearby = True
        self.prohibitsVictoryInCurrentState = True

    def executeMatchNearbyLogic(self):
        self.board.spawnColorElement(self.x,self.y)
        
    
    
