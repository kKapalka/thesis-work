import random
import csv
import sys as sys
from .BoardElement import BoardElement, StandardColorElement, Freckles, VerticalStripey, HorizontalStripey, Icing1, WrappedCandy, Blocker
from .Configurations import Configurations

class Board(object):

    def extractBitData(self, code, bits, pos, baseBitLength = 20):
        binary = bin(code)[2:].zfill(baseBitLength)
        end = len(binary) - pos
        start = end - bits + 1
        kBitSubStr = binary[start : end+1]
        return int(kBitSubStr, 2)

    def __init__(self):
        self.board = []
        self.moveCount = 0
        self.score = 0
        self.moveCountThreshold = 30
        self.matchFound = False
        self.seed = random.randrange(10000, 100000000)
        random.seed(self.seed)
        pass

    def getVictoryState(self):
        if(self.moveCount >= self.moveCountThreshold):
            return -1
        else:
            if(self.score >= self.scoreThreshold):
                victoryProhibited = False
                for row in self.board:
                    for element in row:
                        if (element.prohibitsVictoryInCurrentState):
                            victoryProhibited = True
                            break
                if (victoryProhibited):
                    return 0
                else:
                    return 1
            else:
                return 0
    
    def calculateScoreThreshold(self):
        #self.scoreThreshold = Configurations().getScoreThreshold(self.width, self.height, self.validColors, self.moveThreshold) #training mode
        self.scoreThreshold = 9999999 #evaluation mode

    def updateValidColors(self, validColors):
        self.validColors = validColors
        self.calculateScoreThreshold()

    def initializeFromConfigurations(self):
        #config = Configurations().getRandom() #training mode
        config = Configurations().configs[460] #evaluation mode
        self.initializeFromParams(config[0], config[1], config[2], config[3])

    def initializeFromParams(self, width, height, validColors, moveThreshold):
        self.width = width
        self.height = height
        self.validColors = validColors
        for i in range(0, height):
            line = []
            for j in range(0, width):
                invalidColors = []
                if(j > 1):
                    if(line[j-1].color == line[j-2].color):
                        invalidColors.append(line[j-1].color) 
                if(i > 1):
                    if(self.board[i-1][j].color == self.board[i-2][j].color):
                        invalidColors.append(self.board[i-1][j].color)
                color = random.randrange(1, validColors+1)
                while(color in invalidColors):
                    color = random.randrange(1, validColors+1)
                line.append(StandardColorElement(color, self, j, i))
            self.board.append(line)
        self.moveCountThreshold = moveThreshold
        self.calculateScoreThreshold()
        self.findPotentialMatches()

    def initializeFromLines(self, lines):
        validColors = ["0","1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]
        colors = ["0"]
        self.height = len(lines)
        self.width = 0
        for line in lines:
            self.width = max(self.width, len(line))
        for i in range(0, len(lines)):
            line = []
            for j in range(0, len(lines[i])):
                if(lines[i][j] in validColors):
                    if(lines[i][j] not in colors):
                        colors.append(lines[i][j])
                    line.append(StandardColorElement(validColors.index(lines[i][j]), self, j, i))
                elif (lines[i][j] == "F"):
                    line.append(Freckles(0, self, j, i))
                elif (lines[i][j] == "I"):
                    line.append(Icing1(0, self, j, i))
            if(len(lines[i]) < self.width):
                for j in range(len(lines[i]), self.width):
                    line.append(StandardColorElement(validColors.index(random.randrange(1, len(colors))), self, j, i))
            self.board.append(line)
        self.validColors = len(colors)
        self.calculateScoreThreshold()

    def initializeFromBoardState(self, codeArray, moves, moveCountThreshold = 30,
                                 score = 0, seed = None, previousColorCount = 0):
        self.moveCount = moves
        self.moveCountThreshold = moveCountThreshold
        self.score = score
        colors = []
        self.width = 0
        self.height = 0
        self.board = []
        for j in range(len(codeArray)):
            row = codeArray[j]
            boardLine = []
            for i in range(len(row)):
                element = row[i]
                if(element == 0):
                    element = Blocker(0, self, i, j)
                else:
                    element, color = self.produceElementFromCode(element, j, i)
                    if(color not in colors):
                        colors.append(color)  
                boardLine.append(element)
            if(self.width < len(boardLine)):
               self.width = len(boardLine)
            self.height = self.height + 1
            self.board.append(boardLine)
        self.validColors = len(colors)
        if(seed is not None):
            self.seed = seed
            random.seed(seed)
        if(previousColorCount != 0):
            self.validColors = max(self.validColors, previousColorCount)
        self.calculateScoreThreshold()
        self.findPotentialMatches()
        return 0, 0

    def produceElementFromCode(self, code, yPos, xPos):
        colorData = self.extractBitData(code, 4, 17)
        moveData = self.extractBitData(code, 4, 13)
        moveDataArray = [moveData >= 8, moveData % 8 >= 4, moveData % 4 >= 2, moveData % 2 == 1]
        powerData = self.extractBitData(code, 4, 9)
        flagData = self.extractBitData(code, 8, 1)
        flagDataArray = [flagData >= 128, flagData % 128 >= 64, flagData % 64 >= 32,flagData % 32 >= 16, flagData % 16 >= 8, flagData % 8 >= 4, flagData % 4 >= 2, flagData % 2 == 1]
        if(flagDataArray == [False, False, True, False, True, False, False, False] and colorData > 0):
            if(powerData == 5):
                element = StandardColorElement(colorData, self, xPos, yPos)
            elif(powerData == 7):
                element = WrappedCandy(colorData, self, xPos, yPos)
            elif(powerData == 8):
                element = VerticalStripey(colorData, self, xPos, yPos)
            elif(powerData == 10):
                element = HorizontalStripey(colorData, self, xPos, yPos)
            else:
                print(powerData)

        elif (flagDataArray == [True, False, True, False, False, True, False, False] and powerData == 3):
            element = Icing1(0, self, xPos, yPos)

        elif(flagDataArray == [False, False, True, True, False, False, False, False] and powerData == 15):
            element = Freckles(0, self, xPos, yPos)
        

        elif(flagDataArray == [False, False, False, False, False, False, False, False] and powerData == 0):
            element = Blocker(0, self, xPos, yPos)
        else:
            print(code)
            print(flagDataArray)
            print(colorData)
            print(powerData)
            print(moveDataArray)
        element.validMoves = moveDataArray
        return element, colorData



                                                    
        

    def print(self):
        for line in self.board:
            for element in line:
                print('  ' + ('^' if element.moveWouldResultInAMatch[0] else ' ') +  '  ', end = '')
            print(' ')
            for element in line:
                print(' ' + ('<' if element.moveWouldResultInAMatch[3] else ' ') + str(element.color) + ('>' if element.moveWouldResultInAMatch[1] else ' ') + ' ', end = '')
            print(' ')
            for element in line:
                print('  ' + ('v' if element.moveWouldResultInAMatch[2] else ' ') + '  ', end = '')
            print(' ')
            
    def getCode(self, y, x):
        
        return self.board[y][x].createHash()

    def findPotentialMatches(self):
        self.matchFound = False
        for i in range(0, self.width):
            for j in range(0, self.height):
                self.board[j][i].moveWouldResultInAMatch = [False, False, False, False]
        for i in range(0, self.width):
            for j in range(0, self.height):
                element = self.board[j][i]
                checkMatrix = []
                if(j < self.height - 1 and element.canBeMatchedWith(self.board[j+1][i])):
                    checkMatrix.append((([j-1, i-1], 1), [j+1, i]))
                    checkMatrix.append((([j-1, i+1], 3), [j+1, i]))
                    checkMatrix.append((([j - 2, i], 2), [j+1, i]))
                    checkMatrix.append((([j+2, i-1], 1), [j+1, i]))
                    checkMatrix.append((([j+2, i+1], 3), [j+1, i]))
                    checkMatrix.append((([j + 3, i], 0), [j+1, i]))
                if(j < self.height - 2 and element.canBeMatchedWith(self.board[j + 2][i])):
                    checkMatrix.append((([j+1, i-1], 1), [j+2, i]))
                    checkMatrix.append((([j+1, i+1], 3), [j+2, i]))
                if(i < self.width - 1 and element.canBeMatchedWith(self.board[j][i+1])):
                    checkMatrix.append((([j-1, i-1], 2), [j, i + 1]))
                    checkMatrix.append((([j+1, i-1], 0), [j, i + 1]))
                    checkMatrix.append((([j, i - 2], 1), [j, i + 1]))
                    checkMatrix.append((([j-1, i+2], 2), [j, i + 1]))
                    checkMatrix.append((([j+1, i+2], 0), [j, i + 1]))
                    checkMatrix.append((([j, i + 3], 3), [j, i + 1]))
                if(i < self.width - 2 and element.canBeMatchedWith(self.board[j][i+2])):
                    checkMatrix.append((([j-1, i+1], 2), [j, i + 2]))
                    checkMatrix.append((([j+1, i+1], 0), [j, i + 2]))
                self.runCheckingAgainstCheckMatrix(element, checkMatrix)
        if(not self.matchFound):
            print("Reshuffle...")
            self.shuffleColorfulElements()
            

    def shuffleColorfulElements(self):
        #print("---")
        #print("No potential matches found. Shuffling...")
        #print("---")
        for i in range(0, self.height):
            for j in range(0, self.width):
                if(self.board[i][j].color != 0):
                    invalidColors = []
                    if(j > 1):
                        if(self.board[i][j-1].color == self.board[i][j-2].color and self.board[i][j-1].color != 0):
                            invalidColors.append(self.board[i][j-1].color) 
                    if(i > 1):
                        if(self.board[i-1][j].color == self.board[i-2][j].color and self.board[i][j-1].color != 0):
                            invalidColors.append(self.board[i-1][j].color)
                    color = random.randrange(1, self.validColors+1)
                    while(color in invalidColors):
                        color = random.randrange(1, self.validColors+1)
                    self.board[i][j].color = color
        self.findPotentialMatches()
                
    def runCheckingAgainstCheckMatrix(self, element, checkMatrix):
        
        validChecks = [check for check in checkMatrix if check[0][0][0] >= 0 and check[0][0][0] < self.height and check[0][0][1] >= 0 and check[0][0][1] < self.width]
        for check in validChecks:
            otherChecked = self.board[check[1][0]][check[1][1]]
            candidate = self.board[check[0][0][0]][check[0][0][1]]
            if(element.canBeMatchedWith(candidate) and candidate.canBeMatchedWith(otherChecked) and element.canBeMatchedWith(otherChecked)):
                x = candidate.x
                y = candidate.y
                if (check[0][1] == 0):
                    y = y - 1
                elif (check[0][1] == 1):
                    x = x + 1
                elif (check[0][1] == 2):
                    y = y + 1
                elif (check[0][1] == 3):
                    x = x - 1
                candidate.moveWouldResultInAMatch[check[0][1]] = self.board[y][x].validMoves[(check[0][1] + 2) % 4]
                self.matchFound = self.board[y][x].validMoves[(check[0][1] + 2) % 4] or self.matchFound
        

    def shift(self, x, y, direction):
        if (x not in range(0, self.width) or y not in range(0, self.height)):
            return False
        otherX = x
        otherY = y
        element = self.board[y][x]
        if (direction == 'up'):
            if(y <= 0):
                return False
            otherY = y - 1
        elif (direction == 'down'):
            if (y >= self.height - 1):
                return False
            otherY = y + 1
        elif (direction == 'left'):
            if (x <= 0):
                return False
            otherX = x - 1
        elif (direction == 'right'):
            if (x >= self.width - 1):
                return False
            otherX = x + 1
        else:
            return False
        other = self.board[otherY][otherX]
        return element.swap(other)

    def findHorizontalMatchForElement(self, element, j, i):
        match = []
        if(element.matchedHorizontally or j >= self.width - 2):
            return match
        x = j
        y = i
        right1 = self.board[y][x+1]
        right2 = self.board[y][x+2]
        if(element.canBeMatchedWith(right1) and element.canBeMatchedWith(right2) and right1.canBeMatchedWith(right2) and max(element.color, right1.color, right2.color) != 0):
            match.append(element)
            match.append(right1)
            match.append(right2)
            element.matchedHorizontally = True
            right1.matchedHorizontally = True
            right2.matchedHorizontally = True
            if(x < self.width - 3):
                right3 = self.board[y][x+3]
                if(element.canBeMatchedWith(right3) and right1.canBeMatchedWith(right3)):
                    match.append(right3)
                    right3.matchedHorizontally = True
                    if(x < self.width - 4):
                        right4 = self.board[y][x+4]
                        if(element.canBeMatchedWith(right4) and right1.canBeMatchedWith(right4)):
                            match.append(right4)
                            right3.matchedHorizontally = True
        return match

    def findVerticalMatchForElement(self, element, j, i):
        match = []
        if(element.matchedVertically or i >= self.height - 2):
            return match
        x = j
        y = i
        down1 = self.board[y+1][x]
        down2 = self.board[y+2][x]
        if(element.canBeMatchedWith(down1) and element.canBeMatchedWith(down2) and down1.canBeMatchedWith(down2) and max(element.color, down1.color, down2.color) != 0):
            match.append(element)
            match.append(down1)
            match.append(down2)
            element.matchedVertically = True
            down1.matchedVertically = True
            down2.matchedVertically = True
            if(y < self.height - 3):
                down3 = self.board[y+3][x]
                if(element.canBeMatchedWith(down3)and down1.canBeMatchedWith(down3)):
                    match.append(down3)
                    down3.matchedVertically = True
                    if(y < self.height - 4):
                        down4 = self.board[y+4][x]
                        if(element.canBeMatchedWith(down4)and down1.canBeMatchedWith(down4)):
                            match.append(down4)
                            down4.matchedVertically = True
        return match
    
    def checkForMatches(self):
        matchList = []
        for i in range(0, self.height):
            for j in range(0, self.width):
                element = self.board[i][j]
                matchList.append(self.findHorizontalMatchForElement(element, j, i))
                matchList.append(self.findVerticalMatchForElement(element, j, i))              
        return [match for match in matchList if len(match) > 0]
    
    def attemptMove(self, element1, element2):
        self.board[element1.y][element1.x] = element2
        self.board[element2.y][element2.x] = element1
        
        
        matches = self.checkForMatches()
        if(len(matches) == 0):
            self.board[element1.y][element1.x] = element1
            self.board[element2.y][element2.x] = element2
            return False
        else:
            x = element2.x
            y = element2.y
            element2.x = element1.x
            element2.y = element1.y
            element1.x = x
            element1.y = y
            self.moveCount = self.moveCount + 1
            for match in matches:
                self.score = self.score + (200 * len(match))
                color = max([element.color for element in match])
                for element in match:
                    element.match(color)
                if(element1 in match):
                    element1.conditionallySpawnNewElement(color, len(match))
                if(element2 in match):
                    element2.conditionallySpawnNewElement(color, len(match))
            self.breakElements()
            self.findPotentialMatches()
            if(self.getVictoryState() == 1):
                # Incentive for faster level completion
                self.score = self.score + ((self.moveCountThreshold - self.moveCount)*3000)
                # safe since no more moves should be performed on the board once victory has been achieved
        return True

    def breakElements(self):
        for i in range(self.height - 1, -1, -1):
            for j in range(self.width - 1, -1, -1):
                if (self.board[i][j].matchesUntilDestruction == 0):
                    offsetI = 1
                    while((i - offsetI > 0 and self.board[i-offsetI][j].matchesUntilDestruction == 0) or not self.board[i-offsetI][j].respondsToGravity):
                        offsetI = offsetI + 1
                    if(i - offsetI >= 0 and self.board[i-offsetI][j].matchesUntilDestruction != 0):
                        element = self.board[i - offsetI][j]
                        self.board[i - offsetI][j] = self.board[i][j]
                        self.board[i][j] = element
                    else:
                        self.board[i][j] = StandardColorElement(random.randrange(1, self.validColors+1), self, j, i)
                elif(self.board[i][j].wouldReactToMatchNearby):
                    self.board[i][j].onMatchNearby()
                    
        for i in range(0, self.height):
            for j in range(0, self.width):
                self.board[i][j].x = j
                self.board[i][j].y = i
        #print("checking for cascade matches...")
        matches = self.checkForMatches()
        if(len(matches) != 0):
            for match in matches:
                #print([str(element.x)+", "+str(element.y)+" - "+str(element.color) for element in match])
                self.score = self.score + (200 * len(match))
                color = max([element.color for element in match])
                for element in match:
                    element.match(color)
                match[random.randrange(0, len(match))].conditionallySpawnNewElement(color, len(match))
            self.breakElements()
        for i in range(0, self.height):
            for j in range(0, self.width):
                self.board[i][j].onEndTurn()
        


    def performRandomSwap(self):
        self.findPotentialMatches()
        swappables = []
        for i in range(0, self.height):
            for j in range(0, self.width):
                if(any(self.board[i][j].moveWouldResultInAMatch)):
                    swappables.append(self.board[i][j])
        swappedElement = swappables[random.randrange(0, len(swappables))]
        i = swappedElement.y
        j = swappedElement.x
        validMoves = []
        if(swappedElement.moveWouldResultInAMatch[0]):
            validMoves.append('up')       
        if(swappedElement.moveWouldResultInAMatch[1]):
            validMoves.append('right')        
        if(swappedElement.moveWouldResultInAMatch[2]):
            validMoves.append('down')        
        if(swappedElement.moveWouldResultInAMatch[3]):
            validMoves.append('left')
        direction = validMoves[random.randrange(0, len(validMoves))]
        # print("swapping element ["+str(swappedElement.x)+", "+str(swappedElement.y)+"] direction: "+direction)
        self.shift(swappedElement.x, swappedElement.y, direction)

    def spawnFreckles(self, x, y):
        self.board[y][x] = Freckles(0, self, x, y)

    
    def spawnWrappedCandy(self, x, y, color):
        self.board[y][x] = WrappedCandy(color, self, x, y)
        #print("Freckles has been spawned in position "+str(x)+", "+str(y))

    def spawnColorElement(self, x, y):
        self.board[y][x] = StandardColorElement(random.randrange(1, self.validColors+1), self, x, y)
        #print("Color element has been spawned in position "+str(x)+", "+str(y))

    def spawnStripey(self, x, y, color, direction):
        if(direction == "H"):
            self.board[y][x] = HorizontalStripey(color, self, x, y)
            #print("Horizontal stripey has been spawned in position "+str(x)+", "+str(y))
        elif(direction == "V"):
            self.board[y][x] = VerticalStripey(color, self, x, y)
            #print("Horizontal stripey has been spawned in position "+str(x)+", "+str(y))

    def toggleNearbyMatchFor(self, x, y):
        positionsToToggle = [(x-1, y-1), (x-1, y+1), (x+1, y-1), (x-1, y+1), (x-1, y), (x, y-1), (x, y+1),(x+1, y)]
        for position in positionsToToggle:
            if(position[0] >= 0 and position[1] >= 0 and position[0] < self.width and position[1] < self.height):
                self.board[position[1]][position[0]].wouldReactToMatchNearby = True

def main():
    board = Board()
