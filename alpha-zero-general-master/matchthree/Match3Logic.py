from .Match3Interface import Match3Interface

class Board():

    def __init__(self, n=15, xOffset = None, yOffset = None, score = 0, colors = 0, moveCount = 0, seed = 0):
        self.n = n
        self.interface = Match3Interface(maxSize = n, xOffset = xOffset, yOffset = yOffset, colors = colors, moveCount = moveCount, seed = 0)
        self.xOffset = self.interface.xOffset
        self.yOffset = self.interface.yOffset
        self.pieces = self.interface.getBoardState()
        self.width = self.interface.width
        self.height = self.interface.height
        self.colors = self.interface.colors
        self.moveCount = self.interface.moveCountThreshold
        self.score = score
        self.seed = seed

    def __getitem__(self, index): 
        return self.pieces[index]

    def get_action_space(self):
        return self.interface.getActionSpace()

    def get_legal_moves(self, verbose):
        return self.interface.getValidMoves(verbose)

    def has_legal_moves(self):
        return len(self.interface.hasValidMoves()) > 0
    
    def is_win(self):
        return self.interface.getVictoryState()

    def get_playout_stats(self):
        return self.interface.getScore()

    def replicate(self, board, moves, seed):
        self.interface.seed = seed
        self.interface.replicate(board, moves, self.score)
        self.pieces = self.interface.getBoardState()
        self.width = self.interface.width
        self.height = self.interface.height

    def execute_move(self, move):
        self.interface.performMatrixMove(move)
        self.pieces = self.interface.getBoardState()
        
    def getMovesPerformed(self):
        return self.interface.getMovesPerformed()

    def get_score(self):
        return self.interface.getScore()

    def get_seed(self):
        return self.interface.seed

