import Arena
from MCTS import MCTS
from matchthree.Match3Game import Match3Game
from matchthree.Match3Players import *
from matchthree.keras.NNet import NNetWrapper as NNet

import numpy as np
from my_utils import *

g = Match3Game(13)

n1 = NNet(g)
n1.load_checkpoint('./weights/','best.pth.tar')
args1 = dotdict({'numMCTSSims': 8, 'cpuct':1.0})
mcts1 = MCTS(g, n1, args1)
n1p = lambda x: np.argmax(mcts1.getActionProb(x, temp=0))

hp = HumanMatch3Player(g).play

arena = Arena.Arena(n1p, hp, g, display=Match3Game.display)

print(arena.playGames(10, verbose=True))

