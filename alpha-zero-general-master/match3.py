from prototype import Board, BoardAIInterface
import gym
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.optimizers import Adam
from rl.agents import DQNAgent
from rl.policy import BoltzmannQPolicy
from rl.memory import SequentialMemory
import numpy as np

import mtcs

boardLines = []
boardLines.append("121342")
boardLines.append("411341")
boardLines.append("334113")
boardLines.append("II414I")
boardLines.append("IIIIII")
boardLines.append("IIIIII")
boardLines.append("IIIIII")
boardLines.append("1IIII1")

board = Board()

board.initializeFromLines(boardLines)
board.findPotentialMatches()


