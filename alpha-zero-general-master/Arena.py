import logging

from tqdm import tqdm

log = logging.getLogger(__name__)
import numpy as np

class Arena():
    """
    An Arena class where any 2 agents can be pit against each other.
    """

    def __init__(self, player1, player2, game, display=None):
        """
        Input:
            player 1,2: two functions that takes board as input, return action
            game: Game object
            display: a function that takes board as input and prints it (e.g.
                     display in othello/OthelloGame). Is necessary for verbose
                     mode.

        see othello/OthelloPlayers.py for an example. See pit.py for pitting
        human players/other baselines with each other.
        """
        self.player1 = player1
        self.player2 = player2
        self.game = game
        self.display = display

    def playGame(self, verbose=False):
        """
        Executes one episode of a game.

        Returns:
            either
                winner: player who won the game (1 if player1, -1 if player2)
            or
                draw result returned from the game that is neither 1, -1, nor 0.
        """
        players = [self.player2, None, self.player1]
        curPlayer = self.player1
        board = self.game.getInitBoard()
        it = 0
        movesPerformed = 0
        while self.game.getGameEnded(board, 1, movesPerformed) == 0:
            it += 1
            if verbose:
                assert self.display
                print("Turn ", str(it), "Player 1")
                self.display(board)
            action = curPlayer(self.game.getCanonicalForm(board, 1))

            valids = self.game.getValidMoves(self.game.getCanonicalForm(board, 1), 1)

            if valids[action] == 0:
                log.error(f'Action {action} is not valid!')
                log.debug(f'valids = {valids}')
                assert valids[action] > 0
            if(verbose):
                line = int(action / (self.game.n * 2 - 1))
                digit = action % (self.game.n * 2 - 1)
                useDownMove = digit - (self.game.n - 1) > 0

                moveX = (digit - (self.game.n - 1) if (useDownMove) else digit)
                moveY = line
                print("X - "+str(moveX)+", Y - "+str(moveY)+", direction - "+ ("down" if useDownMove else "right") + " - "+str(action))
            storedMovesPerformed = movesPerformed
            board, _, movesPerformed = self.game.performNextAction(board, 1, action)
        if verbose:
            assert self.display
            print("Game over: Turn ", str(it), "Result ", str(self.game.getGameEnded(board, 1, movesPerformed)))
            self.display(board)
        gameEndedP1 = self.game.getGameEnded(board,1, movesPerformed)
        movesPerformed = 0
        score = self.game.getScore()
        print(score, gameEndedP1)
        curPlayer = self.player2
        board = self.game.getInitBoard(True)
        it = 0
        while self.game.getGameEnded(board, -1, movesPerformed) == 0:
            it += 1
            if verbose:
                assert self.display
                print("Turn ", str(it), "Player 2")
                self.display(board)
            action = curPlayer(self.game.getCanonicalForm(board, -1))

            valids = self.game.getValidMoves(self.game.getCanonicalForm(board, -1), 1)

            if valids[action] == 0:
                log.error(f'Action {action} is not valid!')
                log.debug(f'valids = {valids}')
                assert valids[action] > 0
            if(verbose):
                line = int(action / (self.game.n * 2 - 1))
                digit = action % (self.game.n * 2 - 1)
                useDownMove = digit - (self.game.n - 1) > 0

                moveX = (digit - (self.game.n - 1) if (useDownMove) else digit)
                moveY = line
                print("X - "+str(moveX)+", Y - "+str(moveY)+", direction - "+ ("down" if useDownMove else "right") + " - "+str(action))
            storedMovesPerformed = movesPerformed
            board, _, movesPerformed = self.game.performNextAction(board, -1, action)
        if verbose:
            assert self.display
            print("Game over: Turn ", str(it), "Result ", str(self.game.getGameEnded(board, 1, movesPerformed)))
            self.display(board)
        gameEndedP2 = self.game.getGameEnded(board,curPlayer, movesPerformed)
        print(self.game.getScore(), gameEndedP2)
        if(gameEndedP1 > gameEndedP2):
            return 1
        else:
            score2 = self.game.getScore()
            if(score > score2):
                return 1
            elif(score < score2):
                return -1
            else:
                return 0

    def start(self):
        self.game.getInitBoard()
    
    def calculateNextAction(self, smallBoard, verbose=False):
        self.game.getInitBoard()
        board = np.zeros((13, 13), dtype = np.int_)
        for i in range(len(smallBoard)):
            for j in range(len(smallBoard[i])):
                board[i][j] = smallBoard[i][j]
        print(board)
        return self.player1(self.game.getCanonicalForm(board, 1))


    def playGames(self, num, verbose=False):
        """
        Plays num games in which player1 starts num/2 games and player2 starts
        num/2 games.

        Returns:
            oneWon: games won by player1
            twoWon: games won by player2
            draws:  games won by nobody
        """

        num = int(num)
        oneWon = 0
        twoWon = 0
        draws = 0
        for _ in tqdm(range(num), desc="Arena.playGames (1)"):
            gameResult = self.playGame(verbose=verbose)
            print(gameResult)
            if gameResult == 1:
                oneWon += 1
            elif gameResult == -1:
                twoWon += 1
            else:
                draws += 1

        return oneWon, twoWon, draws
